##############################################################################
# volcano.R
# Make a volcano plot, or the data needed for one
##############################################################################
library(ggplot2)
library(ggrepel)
# Example:

example.volcano.3 <- function(){
  read.delim("proteinGroups.txt", stringsAsFactors = FALSE) -> pg
  
  # LFQ.intensity.10.MJi8.C9.HR3.p16    LFQ.intensity.11.MJi8.C9.HR2.p17 
  # 100                                 101 
  # LFQ.intensity.12.Mji9.C1.12.HR5.p12    LFQ.intensity.13.Mji9.C5.HR2.p14 
  # 102                                 103 
  # LFQ.intensity.1.DR3.HR1.p12        LFQ.intensity.2.34D6.HR4.p12 
  # 104                                 105 
  # LFQ.intensity.3.G1745.C3.HR1.p12    LFQ.intensity.4.G1901.C2.HR1.p18 
  # 106                                 107 
  # LFQ.intensity.5.G1901.C7.HR2.p19   LFQ.intensity.6.MJi7.C1.1.HR1.p10 
  # 108                                 109 
  # LFQ.intensity.7.MJi7C7HR1p14  LFQ.intensity.8.MJi7.C1.10.HR1.p11 
  # 110                                 111 
  # LFQ.intensity.9.MJi8.C8.HR2.p15 
  # 112 
  # 
  i2 <- find.lfq.column.indices(pg,".*(D|G).*") # look up indices
  i1 <- find.lfq.column.indices(pg,".*M[Jj]i.*")
  title <- "MJi vs control"
  
  
  # exclude 102: LFQ.intensity.12.Mji9.C1.12.HR5.p12
  # i1[i1==102] <- NA
  # i1 <- na.exclude(i1)
  # title <- "MJi vs control (excl. 12.Mji9.C1.12.HR5.p12)"
  
  print (title)
  
  p <- volcano(pg,    # protein groups table
               i1, i2, # column indices for each condition
               title, # a title for the plot
               label="unique-------", # label this no matter what
               label.log2ratio.right=4, 
               label.log2ratio.left = 4, # cut out low-sig labels
               label.top = 0 # these conditions are ORed
  )
  print(p)
}=

example.volcano.2 <- function(){
  read.delim("proteinGroups.txt", stringsAsFactors = FALSE) -> pg
  conds <- get.condition.indices(pg) # guesses groupings of your samples!
  ## pick a name, (or combine more, e.g. i1 <- c(conds$BirA,conds$vector))
  ## or let's do it programmatically...
  
  i2 <- c(conds$BirA,conds$vector) # by name
  i1 <- c(conds$tmem98.wt,conds$tmem98.m1)
  
  title <- "tmem98/background"
  print (title)
  
  p <- volcano(pg,    # protein groups table
               i1, i2, # column indices for each condition
               title, # a title for the plot
               label="TMEM98", # label this no matter what
               label.log2ratio.left = 1000, # cut out low-sig labels
               label.top = 0 # these conditions are ORed
  )
  print(p)
}

example.volcano <- function(){
  read.delim("proteinGroups.txt", stringsAsFactors = FALSE) -> pg
  conds <- get.condition.indices(pg) # guesses groupings of your samples!
  ## pick a name, (or combine more, e.g. i1 <- c(conds$BirA,conds$vector))
  ## or let's do it programmatically...
  
  # i1 <- c(conds$BirA,conds$vector) # by name
  
  x = 1
  y = 2
  
  i1 <- conds[[x]]
  i2 <- conds[[y]] # by number
  title <- paste(names(conds)[c(x,y)],collapse="/")
  print (title)
  
  p <- volcano(pg, i1, i2, title, label="TMEM98")
  print(p)
}

volcano <- function(pg, i1, i2, title = "untitled", 
                    label.top=40, 
                    label.log2ratio.left=2,
                    label.log2ratio.right=2, 
                    label.p.value=0.01, 
                    label){
  
  okay <- okay.for.stats(pg, i1, i2, min_n = 2) # usable row indices - requires at least 2 values in one or other condition
  pg[,c(i1,i2)] <- impute(pg[,c(i1,i2)]) # now we can impute values
  
  log2ratio <- log2meanratio(pg[okay,], i1, i2) # x axis
  p.value <- p.values(pg[okay,], i1, i2) # y-axis
  p.adj <- p.adjust(p.value, "fdr") # adjustment for proper filtering!
  
  sig <- rep("(Not Sig.)", length(p.value)) ## labels/colours for significance levels
  sig[p.value<0.05] = "p<0.05"
  sig[p.value<0.01] = "p<0.01"
  sig[p.adj<0.05] = "fdr<0.05"
  
  p.rank <- rank( - log10(p.value)^2 - log2ratio^2) ## can label in halo
  
  name <- pg$Gene.name[okay] # names are the gene names
  
  labelfilter <- unique(c(which(p.rank <= label.top | p.value < label.p.value | log2ratio > label.log2ratio.right | log2ratio < - label.log2ratio.left),
    grep(label, name))) # what to label
  
  x <- data.frame(log2ratio, 
                        p.value, 
                        p.adj, 
                        sig, 
                        name) ## put it together
  
  write.csv(x, "volc-x.csv")
  
  p = ggplot(x, aes(log2ratio, -log10(p.value))) + # plot
    labs(title=title) + # add title
    geom_point(aes(col=sig)) + # add significance categories
    scale_color_manual(values=c("grey","green", "red", "pink")) # and colours
  p + geom_text_repel(data=x[labelfilter,], aes(label = name)) # data labels
}

impute <- function(x){
  x[x == 0] = NA
  minima <- apply(x, 2, min, na.rm=TRUE)
  return(t(
    apply(x, 1, function(row, minima){
      row[is.na(row)] = minima[is.na(row)]
      return (row)
    }, minima)
  ))
}

list.lfq.column.names <- function(x){
  i1 <- find.lfq.column.indices(x, ".*")
  return(names(x)[i1])
}

find.lfq.column.indices <- function(x, pattern){
  pattern = paste("^LFQ.intensity.(",pattern,")$",sep="")
  return(grep(pattern, names(x)))
}

auto.find.conditions <- function(x){
  return(unique(gsub("^LFQ.intensity.(.*)\\.\\d+$","\\1",list.lfq.column.names(x))))
}

get.condition.indices <- function(x){
  a <- auto.find.conditions(x)
  dim(a) <- length(a)
  i <- lapply(a, function(c, x){
    return(find.lfq.column.indices(x, paste(c,".*",sep="")))
  }, x)
  names(i) <- a
  return(i)
}


okay.for.stats <- function(x, indices1=1:3, indices2=4:6, min_n=2){
  n_okay <- apply(x, 1, function(row, i1, i2, min_n){
    #return (is.na(row[i1])
    r1 <- as.numeric(row[i1])
    r2 <- as.numeric(row[i2])
    l1 <- length(which(r1 != 0))
    l2 <- length(which(r2 != 0))
    return(l1 >= min_n || l2 >= min_n)
  }, indices1, indices2, min_n)
  return (which(n_okay))
}

log2meanratio <- function(x, indices1, indices2){
  log2ratio <- apply(x, 1, function(row, i1, i2){
    return(log(mean(as.numeric(row[i1]), na.rm = TRUE)/
               mean(as.numeric(row[i2]), na.rm = TRUE), 2))
  }, indices1, indices2)
  return(log2ratio)
}

p.values <- function(x, indices1, indices2){ 
  p.value <- apply(x, 1, function(row, i1, i2){
    return (
      t.test(as.numeric(row[i1]), 
             as.numeric(row[i2]), 
             alternative = "two.sided", var.equal = TRUE, 
             na.action = "na.omit"
      )$p.value
    )
  }, indices1, indices2)
  return(p.value)
}

