#!perl
use strict;
use warnings;
use File::Find;

my @dirs = @ARGV;
@dirs = ('.') unless @dirs;

my @expect = qw(
    /combined
    /combined/txt
    /combined/txt/proteinGroups.txt
    /combined/txt/proteinGroups.txt.csv
);

foreach my $d(@dirs){
    next unless -d $d;
    opendir(my $dh, $d) or die "could not readdir $d: $!";
    while(my $f = readdir($dh)){
        next unless -d "$d/$f";
        next if $f =~ /^\./;
        foreach my $e(@expect){
            if(! -e $d.'/'.$f.$e){
                print "no $d/$f$e\n";
                last;
            }
        }
    }
}