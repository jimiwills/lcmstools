<<<<<<< HEAD
---
title: "IP Volcano"
author: "jimi.wills@ed.ac.uk"
date: "23 February 2017"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_knit$set(root.dir = "C:/users/jimi/desktop/data/hurd-lab/Toby_2017-02-03_RobotIp_Q2BTH1G/") 

```

## Get data

Change to the directory with the data, read it in, and grab the LFQ columns.  

```{r data}

p <- read.delim("proteinGroups.txt", stringsAsFactors = FALSE)

forwards <- grep("REV__",p$Protein.IDs, invert = TRUE)

lfqi <- grep("LFQ", names(p))
lfq <- p[forwards,lfqi]
samples <- names(lfq) <- sub("LFQ.intensity.","",names(lfq))
print(samples)

```


# Guess the conditions...

We can easily detect which samples are replicates of each other... but can we guess which samples 
represent background and which are tests.  Here are some common names for background controls:

* EV
* Vector
* Vec
* GFP
* BirA
* Empty
* Control

```{r guess}

control.pattern <- "^(EV|Vec|Vector|GFP|BirA|Empty.*|.*Control.*)$"
sample.conditions <- sub(".\\d+$","",samples)
conditions <- unique(sample.conditions)
background.controls <- grep(control.pattern, sample.conditions)
foreground.conditions <- conditions[grep(control.pattern, conditions, invert = TRUE)]
description <- data.frame(i=1:length(samples), samples, sample.conditions)
description$fgbg <- "foreground"
description$fgbg[background.controls] <- "background"
print(description)
print(conditions)

background <- description[description$fgbg == "background",]
foreground <- description[description$fgbg == "foreground",]

print (background)
print (foreground)

```




# normalize

first find the top proteins in the background, then normalize against those...

```{r norm1}

bg.sums <- apply(lfq[background$i], 1, sum)
bg.sums.med <- median(bg.sums)
bg.sums[bg.sums < bg.sums.med] <- NA # now we've the top half only
bg.sums.75 <- median(bg.sums, na.rm = TRUE)
bg.sums[bg.sums < bg.sums.75] <- NA # now we've the top quarter only

norm.rows <- which(! is.na(bg.sums))

print(norm.rows)

norm.sums <- apply(lfq[norm.rows,], 2, sum)
norm.factor <- mean(norm.sums)/norm.sums

lfq <- as.data.frame(t(t(lfq) * norm.factor))


```


# impute

Make 0=NA and imputed versions.

```{r impute}

lfq.na <- lfq
lfq.na[lfq.na == 0] <- NA
boxplot(log(lfq.na,10), las=2, xlab="sample", ylab="log10 label free quantitation intensity", title="LFQ zeroes set to NA")
column.minima <- apply(lfq.na, 2, min, na.rm=TRUE)
print (column.minima)
lfq.imp <- t(
    apply(lfq.na, 1, function(row){
      row[is.na(row)] = column.minima[is.na(row)]
      return (row)
    })
  )
boxplot(log(lfq.imp,10), las=2, xlab="sample", ylab="log10 label free quantitation intensity", title="LFQ imputed")

```

# compare foreground and background

```{r fg/bg}
max.fg.imp <- apply(lfq.imp[,foreground$i], 1, max)
max.bg.imp <- apply(lfq.imp[,background$i], 1, max)
fg_bg <- max.fg.imp/max.bg.imp
# don't allow imputed/imputed
med.fg <- apply(lfq.imp[,foreground$i], 1, median)
med.bg <- apply(lfq.imp[,background$i], 1, median)
med.fg[med.fg==0] <- NA
med.bg[med.bg==0] <- NA
med.fg.or.bg <- med.fg | med.bg
fg_bg[is.na(med.fg.or.bg)] <- NA


boxplot(log(fg_bg,2), las=2, ylab="log2 foreground/background", title="Foreground")


```

We'll use *fg_bg* later to color our volcano plot!  But also to filter the data...

# discard background!

```{r background.discard}

  background.to.discard <- which(fg_bg < 1)


```


# compare test conditions

```{r compare}

comparisons <- combn(foreground.conditions, 2)
print (comparisons)


apply(comparisons, 2, function(pair){
  print(pair)
  name1 <- pair[1]
  name2 <- pair[2]
  column.indices1 <- description$i[description$sample.conditions==name1]
  column.indices2 <- description$i[description$sample.conditions==name2]
  means1 <- apply(lfq.imp[,column.indices1], 1, mean)
  means2 <- apply(lfq.imp[,column.indices2], 1, mean)
  means1_2 <- means1 / means2
  #### DISCARD BACKGROUND
  means1_2[background.to.discard] <- NA
  
  ## no imp/imp:
  meds1 <- apply(lfq[,description$i[description$sample.conditions==name1]], 1, median)
  meds2 <- apply(lfq[,description$i[description$sample.conditions==name2]], 1, median)
  meds1[meds1==0]<-NA
  meds2[meds2==0]<-NA
  meds1or2 <- meds1 | meds2
  means1_2[is.na(meds1or2)]<-NA
  p.value <- apply(lfq.imp, 1, function(row){
    return(
      t.test(
        row[column.indices1],
        row[column.indices2],
        alternative = "two.sided",
        paired = FALSE,
        var.equal = FALSE
        )$p.value
      )
  })
  
  #plot(log(means1_2,2), -log(pvals,10))
  
  maj <- p$Majority.protein.IDs[forwards]
  name <- p$Gene.names[forwards]
  empty.names <- name == ""
  name[empty.names] <- maj[empty.names]

  fg_bg_fl2 <- sprintf("%05d", floor(log(fg_bg,2)))
  fg_bg_fl2[background.to.discard] <- 0

  x <- data.frame(log2ratio = log(means1_2,2),
                        p.value,
                        sig = fg_bg_fl2,
                        name) ## put it together

  mycolours <- colorRampPalette(c("white","grey","cyan","green","blue","red"))
  colours <- mycolours(length(unique(x$sig)))

  print(colours)

  my.title <- paste(name2," v ",name1)
  labelfilter <- which(p.value < 0.01 | x$log2ratio < -4 | x$log2ratio > 4)

    library(ggplot2)
  library(ggrepel)

  
  
  png(paste(my.title,".png",sep=""))
  
   g = ggplot(x, aes(log2ratio, -log10(p.value))) + # plot
     labs(title=my.title) + # add title
     geom_point(aes(col=sig)) + # add significance categories
     scale_color_manual(values=colours) # and colours
  print(g + geom_text_repel(data=x[labelfilter,], aes(label = name))) # data labels

  
  dev.off()
  
   g = ggplot(x, aes(log2ratio, -log10(p.value))) + # plot
     labs(title=my.title) + # add title
     geom_point(aes(col=sig)) + # add significance categories
     scale_color_manual(values=colours) # and colours
  g + geom_text_repel(data=x[labelfilter,], aes(label = name)) # data labels

  
  })
  
  
```

=======
---
title: "IP Volcano"
author: "jimi.wills@ed.ac.uk"
date: "23 February 2017"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_knit$set(root.dir = "C:/users/jimi/desktop/data/hurd-lab/Toby_2017-02-03_RobotIp_Q2BTH1G/") 


```

## Set parameters

```{r params}

definite.labels <- c("RP2")
minimum.fg_bg.requirement <- 2^4

``` 



## Get data

Change to the directory with the data, read it in, and grab the LFQ columns.  

```{r data}

p <- read.delim("proteinGroups.txt", stringsAsFactors = FALSE)

forwards <- grep("REV__",p$Protein.IDs, invert = TRUE)

lfqi <- grep("LFQ", names(p))
lfq <- p[forwards,lfqi]
samples <- names(lfq) <- sub("LFQ.intensity.","",names(lfq))
print(samples)

```


# Guess the conditions...

We can easily detect which samples are replicates of each other... but can we guess which samples 
represent background and which are tests.  Here are some common names for background controls:

* EV
* Vector
* Vec
* GFP
* BirA
* Empty
* Control

```{r guess}

control.pattern <- "^(EV|Vec|Vector|GFP|BirA|Empty.*|.*Control.*)$"
sample.conditions <- sub(".\\d+$","",samples)
conditions <- unique(sample.conditions)
background.controls <- grep(control.pattern, sample.conditions)
foreground.conditions <- conditions[grep(control.pattern, conditions, invert = TRUE)]
description <- data.frame(i=1:length(samples), samples, sample.conditions)
description$fgbg <- "foreground"
description$fgbg[background.controls] <- "background"
print(description)
print(conditions)

background <- description[description$fgbg == "background",]
foreground <- description[description$fgbg == "foreground",]

print (background)
print (foreground)

```




# normalize

first find the top proteins in the background, then normalize against those...

```{r norm1}

bg.sums <- apply(lfq[background$i], 1, sum)
bg.sums[rank(-bg.sums) > 0.25*length(bg.sums)] <- NA # now we've the top quarter only

norm.rows <- which(! is.na(bg.sums))

print(norm.rows)

norm.sums <- apply(lfq[norm.rows,], 2, sum)
norm.factor <- mean(norm.sums)/norm.sums

lfq <- as.data.frame(t(t(lfq) * norm.factor))


```

# normalize part 2

Now we have all samples approx normalized, time to normalize within the foreground samples...

not running this bit... seems to hide rp2...

```{r norm2}
  
  lfq.fgo <- lfq[,foreground$i] # only modify foreground
  
  lfq.fgo[rank(-bg.sums) < 0.5*length(bg.sums), ] <- NA # don't count the top 50% background proteins
  
  lfq.fgo[lfq.fgo == 0] <-NA
  
  fg.col.metric <- apply(lfq.fgo, 2, median, na.rm=TRUE)
  norm.factor <- mean(fg.col.metric)/fg.col.metric
  
  lfq.fg.norm <- as.data.frame(t(t(lfq[,foreground$i]) * norm.factor))
  
  
  lfq[,foreground$i] <- lfq.fg.norm # push results back to data.frame


```


# impute

Make 0=NA and imputed versions.

```{r impute}

lfq.na <- lfq
lfq.na[lfq.na == 0] <- NA
boxplot(log(lfq.na,10), las=2, xlab="sample", ylab="log10 label free quantitation intensity", title="LFQ zeroes set to NA")
column.minima <- apply(lfq.na, 2, min, na.rm=TRUE)
print (column.minima)
lfq.imp <- t(
    apply(lfq.na, 1, function(row){
      row[is.na(row)] = column.minima[is.na(row)]
      return (row)
    })
  )
#boxplot(log(lfq.imp,10), las=2, xlab="sample", ylab="log10 label free quantitation intensity", title="LFQ imputed")

```

# compare foreground and background

```{r fg/bg}
max.fg.imp <- apply(lfq.imp[,foreground$i], 1, max)
max.bg.imp <- apply(lfq.imp[,background$i], 1, max)
fg_bg <- max.fg.imp/max.bg.imp
# don't allow imputed/imputed
med.fg <- apply(lfq.imp[,foreground$i], 1, median)
med.bg <- apply(lfq.imp[,background$i], 1, median)
med.fg[med.fg==0] <- NA
med.bg[med.bg==0] <- NA
med.fg.or.bg <- med.fg | med.bg
fg_bg[is.na(med.fg.or.bg)] <- NA


boxplot(log(fg_bg,2), las=2, ylab="log2 foreground/background", title="Foreground")


```

We'll use *fg_bg* later to color our volcano plot!  But also to filter the data...

# discard background!

```{r background.discard}

  maj <- p$Majority.protein.IDs[forwards]
  name <- p$Gene.names[forwards]
  empty.names <- name == ""
  name[empty.names] <- maj[empty.names]
  definite.label.mask = rep(FALSE, length(name))
  definite.label.mask[match(definite.labels, name)] <- TRUE
  print("Definitely labelled:")
  print(definite.label.mask)
  
  
  
  background.to.discard <- fg_bg < minimum.fg_bg.requirement
  background.to.discard[definite.label.mask] <- FALSE



```


# compare test conditions

```{r compare}

comparisons <- combn(foreground.conditions, 2)
print (comparisons)


apply(comparisons, 2, function(pair){
  print(pair)
  name1 <- pair[1]
  name2 <- pair[2]
  column.indices1 <- description$i[description$sample.conditions==name1]
  column.indices2 <- description$i[description$sample.conditions==name2]
  means1 <- apply(lfq.imp[,column.indices1], 1, mean)
  means2 <- apply(lfq.imp[,column.indices2], 1, mean)
  means1_2 <- means1 / means2
  #### DISCARD BACKGROUND
  means1_2[background.to.discard] <- NA
  means1_2[background.to.discard] <- NA
  
  ## no imp/imp:
  meds1 <- apply(lfq[,description$i[description$sample.conditions==name1]], 1, median)
  meds2 <- apply(lfq[,description$i[description$sample.conditions==name2]], 1, median)
  meds1[meds1==0]<-NA
  meds2[meds2==0]<-NA
  meds1or2 <- meds1 | meds2
  means1_2[is.na(meds1or2)]<-NA
  p.value <- apply(lfq.imp, 1, function(row){
    return(
      t.test(
        row[column.indices1],
        row[column.indices2],
        alternative = "two.sided",
        paired = FALSE,
        var.equal = FALSE
        )$p.value
      )
  })
  
  p.value[is.na(means1_2)] <- NA
  
  #plot(log(means1_2,2), -log(pvals,10))
  

  fg_bg_fl2 <- sprintf("%02d", floor(log(fg_bg,2)))
  fg_bg_fl2[background.to.discard] <- "00"

  x <- data.frame(log2ratio = log(means1_2,2),
                        p.value,
                        log2.over.background = fg_bg_fl2,
                        name) ## put it together

  mycolours <- colorRampPalette(c("white","grey","cyan","green","blue","red"))
  colours <- mycolours(length(unique(x$log2.over.background)))

  print(colours)

  my.title <- paste(name2," v ",name1)
  
  p.value.filter <- rank(p.value) <= 10 # top ten p.values
  #left.filter <- rank(x$log2ratio) <= 10 # top ten LHS proteins
  #right.filter <- rank(-x$log2ratio) <= 10 # top ten RHS proteins
  fgbg.filter <- rank(-fg_bg) <= 10 # top ten foreground proteins
  
  
  #labelfilter <- which(p.value.filter | left.filter | right.filter | fgbg.filter | definite.label.mask)
  labelfilter <- which(p.value.filter | fgbg.filter | definite.label.mask)

    library(ggplot2)
  library(ggrepel)

  
  
  png(paste(my.title,".png",sep=""))
  
   g = ggplot(x, aes(log2ratio, -log10(p.value))) + # plot
     labs(title=my.title) + # add title
     geom_point(aes(col=log2.over.background)) + # add significance categories
     scale_color_manual(values=colours) # and colours
  print(g + geom_text_repel(data=x[labelfilter,], aes(label = name))) # data labels

  
  dev.off()
  
   g = ggplot(x, aes(log2ratio, -log10(p.value))) + # plot
     labs(title=my.title) + # add title
     geom_point(aes(col=log2.over.background)) + # add significance categories
     scale_color_manual(values=colours) # and colours
  g + geom_text_repel(data=x[labelfilter,], aes(label = name)) # data labels

  
  })
  
  
```

>>>>>>> refs/remotes/origin/master
  