# UniProt!

library(UniProt.ws)

mouseUp <- UniProt.ws(10090)

# humanUp <- UniProt.ws(9606)

columns(mouseUp)
keytypes(mouseUp)


# retrieve annotations!
res <- select(mouseUp, keys = c("A2A4P0","A2A5K6"), columns = c("GO","GO-ID","KEGG"), keytype = "UNIPROTKB")

# mod Maj ... cheet and just take first:
ids = gsub(";.*", "", proteinGroups$Majority.protein.IDs)
# ignore REVs
ids = ids[! substr(ids,1,5) == "REV__"]
# remove contaminant prefix? (though it won't be in the same species!)
ids = gsub("CON__","",ids)
# so...
res <- select(mouseUp, keys = ids, columns = c("GO"), keytype = "UNIPROTKB")

# put each GO term on a separate row
s <- strsplit(as.character(res$GO), '; ')
go <- data.frame(GO=unlist(s), UNIPROTKB=rep(res$UNIPROTKB, sapply(s, FUN=length)))
# count them...
freq <- as.data.frame(table(go$GO))
# sort them
freq <- freq[order(-freq$Freq),]

# now we can take a subset of genes and compare...


# first NA the zeroes
proteinGroups$Ci <- proteinGroups$LFQ.intensity.C
proteinGroups$Ci[proteinGroups$Ci == 0] = NA
# then grab the min
minC <- min(proteinGroups$Ci, na.rm = TRUE)
# then set the NAs to the min
proteinGroups$Ci[is.na(proteinGroups$Ci)] = minC

# repeat for G
proteinGroups$Gi <- proteinGroups$LFQ.intensity.G
proteinGroups$Gi[proteinGroups$Gi == 0] = NA
minG <- min(proteinGroups$Gi, na.rm = TRUE)
proteinGroups$Gi[is.na(proteinGroups$Gi)] = minG

# calulate ratios
proteinGroups$ratios <- proteinGroups$Gi / proteinGroups$Ci

# check them:
summary(proteinGroups$Gi)
##      Min.   1st Qu.    Median      Mean   3rd Qu.      Max.
## 3.119e+04 3.094e+06 1.136e+07 2.741e+08 5.122e+07 1.747e+11
summary(proteinGroups$Ci)
##      Min.   1st Qu.    Median      Mean   3rd Qu.      Max.
## 4.087e+04 3.134e+06 1.208e+07 2.360e+08 5.610e+07 9.784e+10
summary(proteinGroups$ratios)
##     Min.  1st Qu.   Median     Mean  3rd Qu.     Max.
##      0.0      0.5      0.8    151.1      1.5 357000.0

# there should not be any Inf or -Inf

# grab IDs where ratio is greater than some amount:
rids <- as.character(proteinGroups$Majority.protein.IDs)[proteinGroups$ratios > 4]
# as earlier...
rids <- gsub(";.*", "", rids)
rids <- rids[! substr(rids,1,5) == "REV__"]
rids <- gsub("CON__","",rids)

# we could look them up, but we already have them, so use merge...
rids <- data.frame(UNIPROTKB = rids)
go$GO <- as.character(go$GO)
go$UNIPROTKB <- as.character(go$UNIPROTKB)

gor <- merge(go, rids, by="UNIPROTKB")
freqr <- as.data.frame(table(gor$GO))
freqr <- freqr[order(-freqr$Freq),]

# rename cols and merge frequency tables
names(freqr) <- c("Var1", "Freqr")
freqr$Var1 = as.character(freqr$Var1)
freqr <- merge(freq, freqr, by="Var1")

# calculate relative incidence
freqr$all <- freqr$Freq / nrow(proteinGroups)
freqr$reg <- freqr$Freqr / nrow(rids)

# and fold overrepresentation
freqr$rep <- freqr$reg / freqr$all

# filter some results...

freqr$Var1[ freqr$Freqr > 10 & freqr$rep > 2 ]



### whatever happened, we now have ~/Downloads/mandyj-imputed-renamed-annot-ratio.csv                                 
### which has:
# [1] "X.1"                  "X"                    "Control.1"                     
# [4] "Control.2"            "Control.3"            "Control.4"                     
# [7] "Control.5"            "MJi7.1"               "MJi7.2"                        
#[10] "MJi7.3"               "MJi8.1"               "MJi8.2"                        
#[13] "MJi8.3"               "MJi9.1"               "MJi9.2"                        
#[16] "Protein.IDs"          "Majority.protein.IDs" "Protein.names"                 
#[19] "Gene.names"           "Fasta.headers"        "ids"                           
#[22] "fw"                   "go"                   "ratio"   
pg = read.csv("/home/jbwills/Mandy_2016-11-02_ProteomicsFasp_POSPL1L/mandyj-imputed-renamed-annot-ratio.csv")  

setwd("C:/Users/Jimi/Desktop/johnstone-lab/Mandy_2016-11-02_ProteomicsFasp_POSPL1L")
pg = read.csv("pgimp-e.csv")  


### so, we need to do a bunch of stuff...
# 1. Grab LFQ columns, and any others of interest
lfqi = grep("Control|MJi",names(pg))

# 2. Replace zeros with NAs
lfq = pg[,lfqi]
lfq[lfq == 0] = NA
# take logs
lfq = log(lfq,base=10)
# 3. Calculate minima for columns
minima = apply(lfq,2,min,na.rm=TRUE)
# 4. Replace zeroes with column minima
lfqimp = as.data.frame(t(apply(lfq,1,function(row, minima){
    rowna = is.na(row)
    row[rowna] = minima[rowna]
    return(row)
}, minima)))
pg[,lfqi] = lfqimp
# 5. Calculate maximal values for the whole data
maxima = apply(lfqimp,1,max,na.rm=TRUE)


# 6. For GO, exclude rows that are reverse, the maximum is too low, there are no values, etc.
mm = min(minima) + 1.7
i = substr(pg$Majority.protein.IDs,0,5) != "REV__" & maxima > mm

# 7. For GO, select which accessions to use for each protein group
acc = gsub(";.*","",pg$Majority.protein.IDs[i])
acc = gsub("CON__","",acc)

# 8. For GO, use UniProt.ws to get annotations
library(UniProt.ws)
humanUp <- UniProt.ws(9606)
# so...
res <- select(humanUp, keys = acc, columns = c("GO"), keytype = "UNIPROTKB")


# 9. make a pathways list and stats for fgsea
gos = res$GO
names(gos) = gsub(';.*','',pg$Majority.protein.IDs[i])
go = strsplit(as.character(gos), '; ')
goL = lapply(go,length)
goN = rep(names(gos), goL)
go = unlist(go)
go = data.frame(go=as.character(go),acc=as.character(goN))
goQ = unique(go$go)
pw = lapply(goQ, function(i){return(go$acc[go$go==i])})
names(pw) = goQ
# this is now the pathways parameter required for fgsea
# the other thing we need is...


# 10. Determine sets of columns to compare
set1i = grep("Control",names(pg))
set2i = grep("MJi",names(pg))
exclude = grep("MJi9.1",names(pg))
set2i = set2i[set2i != exclude] 

# 11. Calculate ratios (difference on log scale)
ratios = apply(pg, 1, function(row, set1i, set2i){
    return(mean(as.numeric(row[set2i]),na.rm=TRUE)-mean(as.numeric(row[set1i]),na.rm=TRUE))
},set1i,set2i)
# and select the rows we're using...
ratii = ratios[i]
names(ratii) = names(gos)

# 13. Run fgsea
library(fgsea)
f = fgsea(pathways=pw, stats=ratii, minSize=5, maxSize=500, nperm=10000)
# turn it into a data.frame so we can output it...
d <- data.frame(f$pathway, f$pval, f$padj, f$ES, f$NES, f$nMoreExtreme, f$size, 
                leadingEdge=unlist(lapply(f$leadingEdge,paste,collapse=";")))
write.csv(d, "fgsea.csv")


# at this point in this dataset I'm not getting any significant adjusted pvalues
# so I could...
# use the leading edge column to look up lfqs for each protein in the list and
# report the median for each GO term before clustering/pca to determine the sample
# relationships of the GO terms.


# 14. PCA

jimiPCA = function(p,i,j){
  pca = princomp(p)
  plot(pca$loadings[,i],pca$loadings[,j], type="n")
  text(pca$loadings[,i],pca$loadings[,j], label=names(p), cex=0.7)
}

jimiPCA(pg[i,c(set1i,set2i)],7,8)



#### NEXT...
# re-order these steps so column-of-interest definition and ratio calculation
# are after annotation, so I can explore the impact of certain columns on 
# gsea result...

save.image()
