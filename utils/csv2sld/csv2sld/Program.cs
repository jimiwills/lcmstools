﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XCALIBURFILESLib;
using Microsoft.VisualBasic.FileIO;

namespace WriteThermoSDL
{
    public class CSVSequence
    {
        public List<string[]> rows = new List<string[]>();
        public int bracket = 0;
        public Dictionary<String, int> indices = new Dictionary<String, int>();
        public string[] userLabels = { "", "", "", "", "" , "" };

        public long Length()
        {
            return rows.LongCount();
        }

        public string GetString(int rowIndex, string fieldName)
        {
            if (! indices.ContainsKey(fieldName))
            {
                Console.WriteLine("<Key not found in header: [" + fieldName + "]>");
                return "<Key not found in header: ["+fieldName+"]>";
            }
            int i = indices[fieldName];
            return "" + rows[rowIndex][i];
        }

        public int GetInt(int rowIndex, string fieldName)
        {
            int i = indices[fieldName];
            int j;
            if (Int32.TryParse(rows[rowIndex][i], out j))
            {
                return j;
            }
            else
            {
                return -1;
            }
        }

        public void ReadCSV(String csvFileName)
        {
            TextFieldParser parser = new TextFieldParser(csvFileName);
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");

            string[] bracketrow = parser.ReadFields();
            // bracket should be in the format Bracket Type=[0-4]
            int b;
            if(Int32.TryParse(bracketrow[0][13].ToString(), out b) && b >= 0 && b <= 4)
            {
                bracket = b;
            }

            string[] header = parser.ReadFields();
            int i = 0;

            foreach (string name in header)
            {
                if (name == "") continue;
                string key = name;
                int j;
                if (key[0] == 'L' && key[2] == ' ' && Int32.TryParse(key[1].ToString(), out j) && j >= 1 && j <= 5)
                {
                    userLabels[j] = name;
                    key = "L" + j;
                }
                indices[key] = i;
                i++;
            }
            while (!parser.EndOfData)
            {
                //Process row
                string[] fields = parser.ReadFields();
                rows.Add(fields);

            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string csvFilename = args.Last();
            string sldFilename = csvFilename + ".sld";
            CSVSequence csv = new CSVSequence();
            csv.ReadCSV(csvFilename);


            WriteSequence(csv, sldFilename);
        }



        private static void WriteSequence(CSVSequence csv, string sldFilename)
        {
            XSequence mySequence = new XSequence(); // define
            mySequence.New(); // initialize
            mySequence.BracketType = XBracketTypes.XOpen; // xopen = 4

            // read user labels
            for (short l = 1; l <= 5; l++)
            {
                mySequence.UserLabel[l] = csv.userLabels[l];
            }

            mySequence.TrayConfiguration = "Conventional";

            XSamples mySamples = mySequence.Samples;
           
            Dictionary<string, XSampleTypes> sampleTypes = new Dictionary<string, XSampleTypes>();
            sampleTypes["Unknown"] = XSampleTypes.XSampleUnknown;
            sampleTypes["Blank"] = XSampleTypes.XSampleBlank;
            sampleTypes["QC"] = XSampleTypes.XSampleQC;

            for (int j = 0; j < csv.Length(); j++)
            {

                XSample mySample = mySamples.Add(-1);


                // assign values to all columns in the sequence for the new sample row
                mySample.SampleType = sampleTypes[csv.GetString(j, "Sample Type")]; // 0=unknown, 1=blank, 2=qc
                mySample.FileName = csv.GetString(j, "File Name");
                mySample.SampleId = csv.GetString(j, "Sample ID");
                mySample.Path = csv.GetString(j, "Path");
                mySample.InstMeth = csv.GetString(j, "Instrument Method");
                mySample.ProcMeth = csv.GetString(j, "Process Method");
                mySample.Position = csv.GetString(j, "Position");
                mySample.InjVol = csv.GetInt(j, "Inj Vol");
                mySample.Level = csv.GetString(j, "Level");
                mySample.CalFile = csv.GetString(j, "Calibration File");
                mySample.Comment = csv.GetString(j, "Comment");
                mySample.DilFactor = csv.GetInt(j, "Dil Factor");
                mySample.ISTDAmt = csv.GetInt(j, "ISTD Amt");
                mySample.SampleName = csv.GetString(j, "Sample Name");
                mySample.SampleVol = csv.GetInt(j, "Sample Vol");
                mySample.SampleWt = csv.GetInt(j, "Sample Wt");
                mySample.UserText[1] = csv.GetString(j, "L1");
                mySample.UserText[2] = csv.GetString(j, "L2");
                mySample.UserText[3] = csv.GetString(j, "L3");
                mySample.UserText[4] = csv.GetString(j, "L4");
                mySample.UserText[5] = csv.GetString(j, "L5");
            }
            mySequence.SaveAs(sldFilename, "jimi.wills@igmm.ed.ac.uk", "Converted by csv2sld", 1);
        }
    }
}

// todo: rename project SLD!  Name user columns!
