#!perl

use strict;
use warnings;

foreach my $fn(map {glob $_} @ARGV){
	open(my $fi, '<', $fn) or die "Could not read $fn: $!";
	open(my $fo, '>', "s$fn") or die "Could not write s$fn: $!";
	print STDERR "$fn > s$fn\n";
	while(<$fi>){
		s/(name="preset\sscan\sconfiguration"\svalue=")./${1}1/g;
		print $fo $_;
	}
	close($fo);
	close($fi);
}

