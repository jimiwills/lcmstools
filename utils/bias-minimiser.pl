#!/usr/bin/perl

# bias-minimser.pl

use strict;
use warnings;

my @blocks = map {[split /\s+/]} <>;

print minimise_bias(@blocks);

sub minimise_bias {
	my @blocks = @_;

	my @flat = map {@$_} @blocks;
	my $elements = @flat;
	my $blocks = @blocks;
	my $string = '';
	my $this = 0;
	my $last = '';
	my $blk = 0;
	my $lastgoodstring = '';
	my %badstrings = ();
	my $tries = 0;
	my $laststartthis = 0;
	while($blk < $blocks){
		last if $blk < 0;
		my @bm = @{$blocks[$blk]}; # block members
		my $blocksize = @bm;
		my $startthis = $this;
		foreach my $s(1..$blocksize){
			foreach my $c(1..$blocksize){
				$this = $this % $blocksize;
				my $m = $bm[$this];
				if($string !~ /<$m>\S*$/ && $string !~ /<$last>\s?<$m>/){
					$string .= "<$m>";
					$last = $m;
					last;
				}
				$this++;
			}
		}
		my @currentblocks = split(/\s+/,$string);
		my $lastblock = pop(@currentblocks);
		my $lastblocksize = ($lastblock =~ tr/<//);
		if($lastblocksize < $blocksize){ #  not a full block
			if(exists $badstrings{$string}){ # seen this block before?
				last if $tries > $elements; # don't try forever
				$tries++;
				$string =~ s/\s+\S+$//; # remove the last block (2 blocks removed)
				$blk--; # decrement the block counter
				$startthis = $laststartthis; # reset the starting value of this
				#print "$startthis\n";
			} 
			$badstrings{$string} = 1;
			$string =~ s/\s+\S+$//; # remove the last block
			$this = $startthis + 1;
		}
		else {
			$laststartthis = $startthis;
			$lastgoodstring = $string
			 if length($string) > length($lastgoodstring);
			$blk ++;
		}
		$string .= " ";
	}
	$lastgoodstring =~ s/\s+/\n/g;
	$lastgoodstring =~ s/></\t/g;
	$lastgoodstring =~ s/<|>//g;
	return $lastgoodstring;
}
