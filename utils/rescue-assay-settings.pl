#!perl

use strict;
use warnings;

# rescue-assay-settings.pl

my %settings = ();
my $component = '';
my @names = qw/rt.min rt.max samples threshold/;
while(<DATA>){
    if(/Peak picking for (.*?)\s*$/){
        #print "Component: $1\n";
        $component = $1;
    }
    elsif(/\d\.\s*([^(]+?)\s*\(([^)]+)\)/){
        #print "Setting: $1, $2\n";
        $settings{$1} = $2;
    }
    elsif(/Pick\s+one\:\s+y/){
        print join("\t", $component, map {$settings{$_}} @names)."\n";
    }
}

__DATA__;

---------------------------------------
Peak picking for GalFruGlu

  1. rt.min(13)
  2. rt.max (19)
  3. samples (15)
  4. threshold (1e+06)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: 2
enter new value for rt.max [ 19 ]: 20

---------------------------------------
Peak picking for GalFruGlu

  1. rt.min(13)
  2. rt.max (20)
  3. samples (15)
  4. threshold (1e+06)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: y
peak1  is joined to  peak2 

---------------------------------------
Peak picking for Fructose-6-phosphate,Glucose 6 phosphate

  1. rt.min(14)
  2. rt.max (18)
  3. samples (15)
  4. threshold (5e+05)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: y
peak1  is joined to  peak2 

---------------------------------------
Peak picking for Fructose 1, 6 bisphosphate

  1. rt.min(14)
  2. rt.max (19)
  3. samples (30)
  4. threshold (50000)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: 3
enter new value for samples [ 30 ]: 15

---------------------------------------
Peak picking for Fructose 1, 6 bisphosphate

  1. rt.min(14)
  2. rt.max (19)
  3. samples (15)
  4. threshold (50000)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: y

---------------------------------------
Peak picking for serine

  1. rt.min(13)
  2. rt.max (18)
  3. samples (15)
  4. threshold (1e+06)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: y

---------------------------------------
Peak picking for Phosphoenol pyruvate

  1. rt.min(14)
  2. rt.max (18)
  3. samples (10)
  4. threshold (5e+05)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: y

---------------------------------------
Peak picking for pyruvate

  1. rt.min(6)
  2. rt.max (16)
  3. samples (11)
  4. threshold (1e+06)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: 3
enter new value for samples [ 11 ]: 6

---------------------------------------
Peak picking for pyruvate

  1. rt.min(6)
  2. rt.max (16)
  3. samples (6)
  4. threshold (1e+06)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: 4
enter new value for threshold [ 1e+06 ]: 5e5
peak1  is joined to  peak2 

---------------------------------------
Peak picking for pyruvate

  1. rt.min(6)
  2. rt.max (16)
  3. samples (6)
  4. threshold (5e+05)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: y
peak1  is joined to  peak2 

---------------------------------------
Peak picking for alanine


These setting did not give any peaks! Please try again... 

  1. rt.min(14)
  2. rt.max (18)
  3. samples (30)
  4. threshold (1e+06)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: a
enter new value for rt.min [ 14 ]: 13
enter new value for rt.max [ 18 ]: 20
enter new value for samples [ 30 ]: 
enter new value for threshold [ 1e+06 ]: 

---------------------------------------
Peak picking for alanine

  1. rt.min(13)
  2. rt.max (20)
  3. samples (30)
  4. threshold (1e+06)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: y

---------------------------------------
Peak picking for lactate

  1. rt.min(6)
  2. rt.max (16)
  3. samples (11)
  4. threshold (1e+06)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: y

---------------------------------------
Peak picking for Citrate,isocitrate

  1. rt.min(14)
  2. rt.max (18)
  3. samples (10)
  4. threshold (1e+06)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: 2
enter new value for rt.max [ 18 ]: 20

---------------------------------------
Peak picking for Citrate,isocitrate

  1. rt.min(14)
  2. rt.max (20)
  3. samples (10)
  4. threshold (1e+06)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: 3
enter new value for samples [ 10 ]: 6
peak2  is joined to  peak3 

---------------------------------------
Peak picking for Citrate,isocitrate

  1. rt.min(14)
  2. rt.max (20)
  3. samples (6)
  4. threshold (1e+06)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: y
peak2  is joined to  peak3 

---------------------------------------
Peak picking for Alpha Ketoglutarate

  1. rt.min(13)
  2. rt.max (17)
  3. samples (10)
  4. threshold (1e+06)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: y

---------------------------------------
Peak picking for Succinate

  1. rt.min(13)
  2. rt.max (17)
  3. samples (20)
  4. threshold (1e+06)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: y
peak2  is joined to  peak3 

---------------------------------------
Peak picking for fumarate

  1. rt.min(6)
  2. rt.max (18)
  3. samples (15)
  4. threshold (50000)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: y
peak2  is joined to  peak3 
peak1  is joined to  peak2 

---------------------------------------
Peak picking for malic acid

  1. rt.min(13)
  2. rt.max (18)
  3. samples (10)
  4. threshold (1e+06)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: 3
enter new value for samples [ 10 ]: 20

---------------------------------------
Peak picking for malic acid

  1. rt.min(13)
  2. rt.max (18)
  3. samples (20)
  4. threshold (1e+06)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: y

---------------------------------------
Peak picking for D-ribulose 5-phosphate, ribose 5 phosphate

  1. rt.min(13)
  2. rt.max (18)
  3. samples (10)
  4. threshold (2e+05)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: y

---------------------------------------
Peak picking for 6 Phospho gluconate

  1. rt.min(14)
  2. rt.max (18)
  3. samples (15)
  4. threshold (5e+05)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: a
enter new value for rt.min [ 14 ]: 10
enter new value for rt.max [ 18 ]: 20
enter new value for samples [ 15 ]: 10
enter new value for threshold [ 5e+05 ]: 

---------------------------------------
Peak picking for 6 Phospho gluconate

  1. rt.min(10)
  2. rt.max (20)
  3. samples (10)
  4. threshold (5e+05)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: 1
enter new value for rt.min [ 10 ]: 12

---------------------------------------
Peak picking for 6 Phospho gluconate

  1. rt.min(12)
  2. rt.max (20)
  3. samples (10)
  4. threshold (5e+05)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: y

---------------------------------------
Peak picking for IMP


These setting did not give any peaks! Please try again... 

  1. rt.min(13)
  2. rt.max (17)
  3. samples (15)
  4. threshold (5e+05)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: y

No peaks.  Skipping


---------------------------------------
Peak picking for AMP

  1. rt.min(12)
  2. rt.max (16)
  3. samples (15)
  4. threshold (5e+05)
  a. Set all.
  y. Yes, it looks OK. Continue.
  q. quit.
Pick one: y
Error in approx(c(med1i, med2i), np[c(med1i, med2i)], start:end) : 
  need at least two non-NA values to interpolate
