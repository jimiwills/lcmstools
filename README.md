# lcmstools README

## assay.R

assay.R is located in the lcmstools/metabololimics folder.  It requires that you have centroided mzML files to work on 
(actually it will work on profile data, but takes longer) and you need to have Proteowizard installed.

There are some sample mzML files in [gitlab.com/jimiwills/assayRdata](https://gitlab.com/jimiwills/assayRdata) .

Say you have an experiment in folder C:\users\jimi\JimisExpt, and your mzML files are in JimisExpt\mzMLneg,
you would first change directory to C:\users\jimi\JimisExpt (in R):

    R> setwd("C:/users/jimi/JimisExpt")

If you just have raw data, you can use msconvert_all() to convert the *.raw files to mzML files in subfolders
mzMLneg and mzMLpos.

    R> msconvert_all()

Next you need a config file, and there are plenty of example tsv files in lcmstools/metabololimics.  Let's say
your config file is called standards-maxC6.tsv.  This example tsv tells assay.R to consider up to 6x 13C for each
configured metabolite.  Take a look.

Now we have the info we need to run the script... and you have two options.

    R> ticpath <- run.config.tics("standards-maxC6.tsv", "mzMLneg") # creates chromatogram file intermediates
    R> results <- run.config.peaks("standards-maxC6.tsv", ticpath) # peak detection and measurements

Or, a shortcut for both:

    R> results <- run.config("standards-maxC6.tsv", "mzMLneg") # does both

run.config.peaks outputs a lot of XICs to the current directory (as PNGs).  It shows you these XICs in interactive mode
(if value in "interactive" column of config is "yes") as it progresses, and you can tweak the peak detection settings.
At the end, it also outputs a fresh config with your tweaks.

The resulting table can be output... 

    R> write.csv(results, "results-neg.csv")

And there's a function that makes stacked bar plots and a nicer table too:

    R> better.table <- assay.plotter(results)

At present, it outputs the table to the current directory, in a file called "p2-results.csv".


Please email jimi.wills@ed.ac.uk with any questions.


# The notes below are now a little out of date.

This file will hopefully contain everything you need to know to run these tools.

## Notes

Dear Team, here are some note to keep you straight.

### License

Before contributing to this project, please review understand and agree to the License (in the file called LICENSE)

I chose the MIT license after visiting [choosealicense.com](http://choosealicense.com/)

### Git

If you have problems using git, let me (Jimi) know and I'll help you.

Remember to Tools->Version Control->Pull Branches (in RStudio) everytime you use this project in order to 
see, inspect, understand, benefit from the latest changes.  If you're more of a keyboard person, you can
Alt-T V P instead.

If you are developing on the project, the shortcut for commiting your changes is Ctrl-Alt-M.  Then remember to
select the files to update (or press the Stage button to select them automatically) and type a commit message
describing the changes you made so other users know what happened.

### Markdown

This document is written in a format called markdown.  Both RStudio and Sublime Text 
understand Markdown and will do some syntak highlighting.  But the whole point of 
markdown is that it is readable as plain text.  Headers are denoted by hash (#), lists
by hyphen (-) and code by indentation (4 or more spaces I think).  

More info on Markdown is available: 

[Here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
[And here](https://gitlab.com/gitlab-org/gitlab-ce/blob/6-4-stable/doc/markdown/markdown.md)
[And maybe in the nicest format here](https://www.cheatography.com/snidd111/cheat-sheets/gitlab-markdown/)

Now you see how links are formatted.

In the left margin of this file (in RStudio) you should see some little triangles, which you can use to collapse/expand
sections of this document.

### The structure of this project

This project is currently a loose association of fairly unrelated scripts that all have something to do with lcms.

If any of them reach a mature enough level to be released independently, we should maintain a link from here to 
the new URL... unless we open this project entirely.  By 'open' I mean make un-private; this project is currently
only viewable by the team working on it, which as of now is myself (Jimi), Joy and Andy.

assay.R is the name of the OldGregg script.  My next todos are:

- [ ] msaccess automatic path detection (so it works on different systems without reconfiguration)
- [ ] document the usage of assay.R (it's really simple though)

But there are more.  And now we know how a markdown todo list is formatted.

## assay.R

Uses Proteowizard's msaccess to read XICs from mzML or other files.
Does simple (but effective) peak detection based on the maximal
profile of all relevant XICs for an ion.  All isotopes in all
conditions are overlayed and the maximum value seen at each time
point is used.  (in our experiments we see a maximum deviation of
about 0.2 seconds between runs on an experiment)
Peak detection is (or can be) interactive, for manual QC/tweaking.
Outputs XICs.
Calculates volume of each peak and gives as a dataframe.
Outputs other graphics.

In order to use this script, 

## bias-minimiser.pl

Reads in replicate blocks line by line, with samples separated by spaces, eg:

    WT-A WT-B WT-C mut-X mut-Y mut-Z EB
    WT-A WT-B WT-C mut-X mut-Y mut-Z EB
    WT-A WT-B WT-C mut-X mut-Y mut-Z
    WT-A WT-B WT-C mut-X mut-Y mut-Z
    WT-A WT-B WT-C mut-X mut-Y mut-Z

Prints out the sample samples, but each block is re-ordered to that no consecutive pair of samples is repeated.
This is not alwasy possible, and the script with output as many blocks as are possible.

This script is also available online at [webu](http://webu.uk/tools/bias-minimiser.php)


## ipms.R

Does some stuff with LFQ data from MaxQuant... including comparisons between conditions, and plots for
specific proteins.

