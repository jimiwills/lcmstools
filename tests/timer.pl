#!perl

$|++;

my @mzs = qw/
	122.0808 134.0806 123.0550 393.2845 444.3303 
	227.1128 194.1015 130.1586 212.0943 171.1485
/;
my @mzs = map {sprintf("%.4f,%.4f", $_ * 0.999995, $_ * 1.000005)} @mzs;

my @xs = map {qq!-x "tic mz=$_ delimiter=tab"!} @mzs;

my $together_cmd = join(' ', 'msaccess', @xs, '*.mzML');
my @separate_cmds = map { join(' ', 'msaccess', $_, '*.mzML')} @xs;


print "T: $together_cmd\n";

print map {"$_\n"} "S:", @separate_cmds;

exit;


foreach(1..10){
	print "$_\t";
	my $t = dur(sub{
		`$together_cmd`;
	});
	print "$t\t";
	my $s = dur(sub{
		`$_` foreach @separate_cmds;
	});
	print "$s\t".($t-$s)."\n";
}

sub dur {
	my $start = time();
	$_[0]->();
	return time()-$start;
}