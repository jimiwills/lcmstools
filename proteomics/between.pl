#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use Paths::Graph;

print "start\n";

my $fn = '9606.protein.links.v10.txt';
open(my $fh, '<', $fn) or die "Could not read $fn: $!";
my %db = ();
my %scores = ();
my $minscore = 400;
while(<$fh>){
    next unless m/(\d+\.ENSP\d+)\s+(\d+\.ENSP\d+)\s+(\d+)/;
    next unless $3 > $minscore;
    $db{$1} = {} unless exists $db{$1};
    $db{$1}->{$2} = 1;
    $scores{"$1 $2"} = $3;
    # apparently the db goes both directions
    # so we don't have to

}

print "finished reading\n";
#print Dumper \%db;
# now this happens to be in the format accepted by 
# Paths::Graph... but it takes too long and too much RAM
# we don't need to whole network...
# so find if they can be joined first and then do the shortest route thing...

my $tmem98 = '9606.ENSG00000006042';
my $tmem237 = '9606.ENSP00000386264';
my $WHSC1L1 = '9606.ENSP00000313983';
 
#print subnet($start, $knowntarget, \%db), "\n";
#print subnet($start, $ktsecondary, \%db);
#print map {pathscore($_,\%scores)} subnet($tmem98, $tmem237, \%db), "\n";
#print map {pathscore($_,\%scores)} subnet($WHSC1L1, $tmem98, \%db), "\n";
print map {pathscore($_,\%scores)} subnet($tmem237, $WHSC1L1, \%db), "\n";
exit;


sub pathscore {
    my ($path,$scores) = @_;
    my @path = split(/\n/,$path);
    my $scoredpath = '';
    foreach my $i(1..$#path){
        my ($p1,$p2) = ($path[$i-1], $path[$i]);
        my $s = '';
        if(exists $scores->{"$p1 $p2"}){
            $s = $scores->{"$p1 $p2"};
        }
        $scoredpath .= "$p1 $s\n"
    }
    return $scoredpath;
}

sub subnet {
    my ($start,$target,$net) = @_;
    my @items = ($start);
    #my %done = ($start=>1);
    my %paths = ($start => '');
    my @paths = ();
    while(@items){
        my $this_item = shift @items;
        if($this_item eq $target){
            push @paths, $paths{$this_item} . "\n$target\n---\n";
        }
        else {
            my @ints = keys %{$net->{$this_item}};
            foreach my $int (@ints){
                next if exists $paths{$int};
                $paths{$int} = $paths{$this_item} . "\n" . $this_item;
                push @items, $int;
            }
        }
    }
    return @paths;
}